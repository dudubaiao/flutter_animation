import 'package:animation/src/screens/home.dart';
import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
        title: 'Animation',
        theme: ThemeData(primarySwatch: Colors.blue),
        home: Home(),
      );
}
