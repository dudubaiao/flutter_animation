import 'dart:math' as math;

import 'package:animation/src/widgets/box.dart';
import 'package:animation/src/widgets/cat.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomeState();
}

const flapMargin = 6.0;
const flapAnimationInitialPosition = 0.56;

class HomeState extends State<Home> with TickerProviderStateMixin {
  AnimationController catAnimationController;
  Animation<double> catPositionAnimation;

  AnimationController flapAnimationController;
  Animation<double> flapRotationAnimation;

  @override
  void initState() {
    super.initState();

    initCatAnimation();
    initFlapAnimation();
  }

  void initCatAnimation() {
    catAnimationController = AnimationController(
      duration: Duration(seconds: 1),
      vsync: this,
    );

    catPositionAnimation = Tween(begin: 35.0, end: 80.0).animate(
      CurvedAnimation(
        parent: catAnimationController,
        curve: Curves.easeIn,
      ),
    );
  }

  void initFlapAnimation() {
    flapAnimationController = AnimationController(
      duration: Duration(milliseconds: 500),
      vsync: this,
    );

    flapRotationAnimation = Tween(begin: 0.0, end: 0.04).animate(
      CurvedAnimation(
        parent: flapAnimationController,
        curve: Curves.easeInOut,
      ),
    );

    flapRotationAnimation.addStatusListener((status) {
      if (status == AnimationStatus.dismissed) {
        flapAnimationController.forward();
      } else if (status == AnimationStatus.completed) {
        flapAnimationController.reverse();
      }
    });

    flapAnimationController.forward();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text('Animation'),
        ),
        body: GestureDetector(
          child: Center(
            child: Stack(
              children: [
                _buildCat(),
                Box(),
                _buildLeftFlap(),
                _buildRightFlap(),
              ],
              overflow: Overflow.visible,
            ),
          ),
          onTap: _onTap,
        ),
      );

  Widget _buildCat() => AnimatedBuilder(
        child: Cat(),
        animation: catPositionAnimation,
        builder: (BuildContext context, Widget child) {
          return Positioned(
            child: child,
            left: 0,
            right: 0,
            top: catPositionAnimation.value * -1,
          );
        },
      );

  Widget _buildLeftFlap() {
    return _buildFlap(true);
  }

  Widget _buildRightFlap() {
    return _buildFlap(false);
  }

  Widget _buildFlap(bool isLeft) => Positioned(
        left: isLeft ? flapMargin : null,
        right: !isLeft ? flapMargin : null,
        child: AnimatedBuilder(
          child: Flap(),
          animation: flapRotationAnimation,
          builder: (BuildContext context, Widget child) {
            var angle = (math.pi * (flapAnimationInitialPosition + flapRotationAnimation.value)) * (isLeft ? 1 : -1);

            return Transform.rotate(
              child: child,
              angle: angle,
              alignment: (isLeft ? Alignment.topLeft : Alignment.topRight),
            );
          },
        ),
      );

  _onTap() {
    if (catAnimationController.status == AnimationStatus.dismissed) {
      catAnimationController.forward();
      flapAnimationController.stop();
    } else if (catAnimationController.status == AnimationStatus.completed) {
      catAnimationController.reverse();
      flapAnimationController.forward();
    }
  }
}
