import 'package:flutter/material.dart';

const _boxColor = Colors.brown;

class Box extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200.0,
      height: 200.0,
      color: _boxColor,
    );
  }
}

class Flap extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 125.0,
      height: 10.0,
      color: Colors.red,
    );
  }
}
